/*
 * rotcelloc Epic Games Store exporter bookmarklet - exports a Epic Games Store
 *              collection to CSV
 *
 * Part of rotcelloc - hacker's movies, tv-series and games collector
 * application
 *
 * Copyright (C) Eskild Hustvedt 2015, 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * USAGE:
 * Navigate to https://www.epicgames.com/account/transactions
 * Add this script as a bookmarklet and click it, or open up the developer tools
 * and paste the script source there
 *
 * The script will take some time to run, since it needs to load in all of your
 * purchases, which epic paginates. It will output progress to the console.
 */

// jshint esversion: 6
/* global jQuery*/

// Boilerplate for fetching jQuery
// eslint-disable-next-line
(function ($) {
    const result = [];
    const clog = (msg) => {
        console.log("epicGamesExporterBookmarklet: " + msg);
    };
    const tableToJson = function (domTable) {
        const json = [];
        const headers = Array.from(domTable.querySelectorAll("th")).map(
            (th) => th.textContent,
        );
        Array.from(domTable.querySelectorAll("tr"))
            .slice(1)
            .forEach((row) => {
                const rowData = {};
                Array.from(row.querySelectorAll("td")).forEach((td, i) => {
                    rowData[headers[i]] = td.textContent;
                });
                json.push(rowData);
            });
        return json;
    };
    const loadAllPages = (cb) => {
        const performRefresh = () => {
            const button = $("#payment-history-show-more-button");
            if (button.length) {
                if ($(".is-loading").length > 0) {
                    clog("… still loading");
                    setTimeout(performRefresh, 500);
                } else {
                    button.click();
                    clog(
                        'There are more entries, clicking "Show More" to load more...',
                    );
                    setTimeout(performRefresh, 500);
                }
            } else {
                clog(
                    "Everything appears to be loaded, generating CSV and triggering download",
                );
                cb();
            }
        };
        performRefresh();
    };
    loadAllPages(() => {
        for (const entry of tableToJson(
            document.querySelectorAll("table")[0],
        )) {
            result.push(entry.Description + ";PC;Epic Games Store");
        }
        const CSV = "Title;Platform;Format\n" + result.join("\n");
        const $a = $("<a />")
            .attr("download", "epicgamesstore.csv")
            .attr(
                "href",
                "data:text/csv;charset=utf-8," + encodeURIComponent(CSV),
            );
        $a.appendTo("body");
        $a[0].click();
    });
})(jQuery); // end of code
