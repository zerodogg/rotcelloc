/*
 * rotcelloc Humble Bundle exporter bookmarklet - exports the list of
 *          unredeemed Humble Bundle keys to CSV
 *
 * Part of rotcelloc - hacker's movies, tv-series and games collector
 * application
 *
 * Copyright (C) Eskild Hustvedt 2015, 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * USAGE:
 * Navigate to https://www.humblebundle.com/home/keys
 * Add this script as a bookmarklet and click it, or open up the developer
 * tools and paste the script source there
 */

// jshint esversion: 6
/* global jQuery*/

// Boilerplate for fetching jQuery
// eslint-disable-next-line
void function($){var loadBookmarklet=function($){
        // Start of actual code
// eslint-disable-next-line
(function ($){
    const games = [];
    const seen = {};
    const keyTypes = {};
    let pages = 0;
    let pageNo = 1;
    const clog = (msg) =>
    {
        console.log('humbleBundleExporterBookmarklet-unredeemed: '+msg);
    };
	const process = function ()
    {
        const $paginator = jQuery('.pagination > div');
        pages = Number.parseInt(jQuery($paginator[ $paginator.length-2]).text(),10);
		$('.game-name h4').each(function ()
        {
			let name = $(this).text();
			if (! /(Desura|Multiplayer (key|activation)|subscription|Closed Beta|Month License|\d\d\d\d Humble Choice)/i.test(name))
            {
				name = name.replace(/\s*(Steam Keys|\(Steam\)|Beta)+/gi,'')
                        .replace(/(Friend)?\s*Keys?\s*$/i,'')
                        .replace(/Steam\s*$/,'')
                        .replace(/\s+$/,'');
                if(seen[name] === undefined)
                {
                    seen[name] = 0;
                    try
                    {
                        const platform = $(this).first().parent().parent().find('.platform i').attr('title');
                        keyTypes[name] = platform;
                    }
                    catch(e) {}
                }
                seen[name]++;
				games.push(name);
			}
		});
		const $right = $('.js-jump-to-page .hb-chevron-right');
		if($right.length > 0)
		{
            pageNo++;
            clog('Loading page '+pageNo+' of '+pages);
			$right.click();
			setTimeout(process,1000);
		}
		else
		{
			games.sort();
            const result = [];
            const alreadyAdded = {};
			for(const game of games)
            {
                if(!alreadyAdded[game])
                {
                    let entry = game+';PC;Humble Bundle';
                    let note = [];
                    if(seen[game] > 1)
                    {
                        note.push(seen[game]+' keys');
                    }
                    if(keyTypes[game] !== undefined)
                    {
                        note.push('On '+keyTypes[game]);
                    }
                    entry += '; '+note.join(', ');
                    result.push(entry);
                    alreadyAdded[game] = true;
                }
            }
            clog('Got games, downloading CSV');
            const CSV = 'Title;Platform;Format;Note\n'+result.join("\n");
            const $a = $('<a />').attr('download','humblebundle-unredeemed.csv').attr('href','data:text/csv;charset=utf-8,'+ encodeURIComponent(CSV));
            $a.appendTo('body');
            $a[0].click();
			console.log(window.hbglist);
		}
	};
    if (! $('#hide-redeemed').is(':checked'))
    {
        clog('Hiding redeemed games, waiting 1.5 seconds for it to load');
        $('#hide-redeemed').click();
        setTimeout(process,1500);
    }
    else
    {
        process();
    }
})(jQuery); // end of code
// More boilerplate for fetching jQuery
// eslint-disable-next-line
},hasJQuery=$&&$.fn&&parseFloat($.fn.jquery)>=1.7;if(hasJQuery)loadBookmarklet($);else{var s=document.createElement("script");s.src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js",s.onload=s.onreadystatechange=function(){var state=this.readyState;state&&"loaded"!==state&&"complete"!==state||loadBookmarklet(jQuery.noConflict())}}document.getElementsByTagName("head")[0].appendChild(s)}(window.jQuery); // jshint ignore:line
