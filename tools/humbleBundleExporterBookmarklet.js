/*
 * rotcelloc Humble Bundle exporter bookmarklet - exports a Humble Bundle
 *              collection to CSV
 *
 * Part of rotcelloc - hacker's movies, tv-series and games collector
 * application
 *
 * Copyright (C) Eskild Hustvedt 2015, 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * USAGE:
 * Navigate to https://www.humblebundle.com/home/library
 * Add this script as a bookmarklet and click it, or open up the developer
 * tools and paste the script source there
 */

// jshint esversion: 6
/* global jQuery*/

// Boilerplate for fetching jQuery
// eslint-disable-next-line
(function ($){
        const result = [];
        const clog = (msg) => {
            console.log('humbleBundleExporterBookmarklet: '+msg);
        };
        const setPlatform = (platform,cb) => {
            $('#switch-platform').val(platform).trigger('change');
            clog('… waiting for page to settle to the new platform');
            setTimeout(cb,1000);
        };
        const gatherGames = (platform) => {
            let games = [];
            $('.hb-download-list .subproduct-selector').each(function () {
                games.push($(this).find('h2').text());
            });
            if(games.length == 0)
            {
                clog('Found no games on page for '+platform);
            }
            return games;
        };
        clog("Switching to Windows as a platform to get PC games :(");
        let games = {};
        let winGames, androidGames, asmjsGames, linuxGames;
        const finalize = () =>
        {
            for(let game of winGames)
            {
                let entry = game+';PC';
                if(linuxGames.indexOf(game) !== -1)
                {
                    entry += ', Linux';
                }
                if(androidGames.indexOf(game) !== -1)
                {
                    entry += ', Android';
                }
                if(asmjsGames.indexOf(game) !== -1)
                {
                    entry += ', Humble Play';
                }
                entry += ';Humble Bundle';
                result.push(entry);
            }
            for(let game of androidGames)
            {
                if(winGames.indexOf(game) === -1)
                {
                    let entry = game+';Android;Humble Bundle';
                    result.push(entry);
                }
            }
            clog('Got games, downloading CSV');
            const CSV = 'Title;Platform;Format\n'+result.join("\n");
            const $a = $('<a />').attr('download','humblebundle.csv').attr('href','data:text/csv;charset=utf-8,'+ encodeURIComponent(CSV));
            $a.appendTo('body');
            $a[0].click();
        };
        // Welcome to callback hell
        setPlatform("windows", () => {
            winGames = gatherGames("windows");
            clog('Switching to Android to get Android games');
            setPlatform("android", () => {
                androidGames = gatherGames("android");
                clog('Switching to Humble Play to get asmjs games');
                setPlatform("asmjs", () => {
                    asmjsGames = gatherGames("asmjs");
                    clog('Switching to Linux to get those games');
                    setPlatform("linux",() => {
                        linuxGames = gatherGames("asmjs");
                        finalize();
                    });
                });
            });
        });
})(jQuery); // end of code
