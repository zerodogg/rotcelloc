#!/usr/bin/node
const csvParseSync = require("csv-parse/lib/sync");
const fs = require("fs");

function main() {
    // eslint-disable-next-line
    const argv = process.argv;

    const input = argv[2];
    const content = fs.readFileSync(input).toString();
    const filtered = [];
    let seen = false;
    for (const line of content.split("\n")) {
        if (/^Position,/.test(line)) {
            seen = true;
        }
        if (seen) {
            filtered.push(line);
        }
    }
    const CSV = csvParseSync(filtered.join("\n"), {
        delimiter: ",",
        trim: true,
        columns: true,
        relax_column_count: true,
        skip_empty_lines: false,
    });
    const newEntries = [];
    for (const entry of CSV) {
        let format = "DVD";
        if (entry.Description && /BD/i.test(entry.Description)) {
            format = "Blu-ray";
            if (/DVD/i.test(entry.Description)) {
                format += ", DVD";
            }
        }
        newEntries.push(entry.Name + ";" + entry.Year + ";;;" + format);
    }
    fs.writeFileSync("out.csv", newEntries.join("\n"));
}
main();
