/*
 * rotcelloc Nintendo eShop exporter bookmarklet - exports a Nintendo eShop
 *              collection to CSV
 *
 * Part of rotcelloc - hacker's movies, tv-series and games collector
 * application
 *
 * Copyright (C) Eskild Hustvedt 2015, 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * USAGE:
 * Navigate to https://ec.nintendo.com/my/#/transactions/1
 * Add this script as a bookmarklet and click it, or open up the developer tools
 * and paste the script source there
 *
 * The script will take some time to run, since it needs to load in all of your
 * purchases, which epic paginates. It will output progress to the console.
 */

// jshint esversion: 6
/* global jQuery*/

// Boilerplate for fetching jQuery
// eslint-disable-next-line
void function($){var loadBookmarklet=function($){
        // Start of actual code
// eslint-disable-next-line
(function ($){
        const result = [];
        const parsePage = function ()
        {
            $('.o_c-card-history__title').each(function ()
                {
                    const entry = $(this).find('.o_c-card-history__title-name').text().replace(/\n/g,' ');
                    const platform = $(this).find('.o_c-card-history__title-platform').text();
                    if(entry === 'Nintendo 3DS software' || /Switch Online/.test(entry))
                    {
                        return;
                    }
                    result.push(entry+';'+platform+';Nintendo eShop');
                });
            if(!/disable/.test($('.o_c-pager-defult__button').last().attr('class')))
            {
                const next = $('.o_c-pager-defult__button').length-2;
                $('.o_c-pager-defult__button')[next].click();
                const interval = setInterval(() =>
                {
                    if($('.o_c-card-history__title-name').length > 0)
                    {
                        clearInterval(interval);
                        parsePage();
                    }
                },1000);
            }
            else
            {
                const CSV = 'Title;Platform;Format\n'+result.join("\n");
                const $a = $('<a />').attr('download','nintendo-eshop.csv').attr('href','data:text/csv;charset=utf-8,'+ encodeURIComponent(CSV));
                $a.appendTo('body');
                $a[0].click();
            }
        };
        parsePage();
})(jQuery); // end of code
// eslint-disable-next-line
},hasJQuery=$&&$.fn&&parseFloat($.fn.jquery)>=1.7;if(hasJQuery)loadBookmarklet($);else{var s=document.createElement("script");s.src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js",s.onload=s.onreadystatechange=function(){var state=this.readyState;state&&"loaded"!==state&&"complete"!==state||loadBookmarklet(jQuery.noConflict())}}document.getElementsByTagName("head")[0].appendChild(s)}(window.jQuery); // jshint ignore:line
