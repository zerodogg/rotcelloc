#!/usr/bin/perl
# rotcelloc-to-letterboxd.pl
# Part of rotcelloc - the hacker's movie, tv-series and game collection
# manager
# Copyright (C) Eskild Hustvedt 2023
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use 5.030;
use feature 'signatures';
no warnings 'experimental::signatures';
use LWP::Simple;
use JSON;
use IO::All;
use Mojo::CSV;
use Getopt::Long;

# XXX: Will be used to hide certain messages when not in verbose mode
sub printv ($message) {
    say $message;
}

# Fetches and parses JSON either locally or from a deployed website
sub getParsedData ( $domain, $dataset ) {
    my $data;
    my $fullPath = $domain . '/' . $dataset . '.dataset.json';
    if ( index( $domain, 'http' ) == 0 ) {
        $data = get($fullPath) or die;
    }
    else {
        $data = io($fullPath)->slurp or die;
    }
    return from_json($data) or die;
}

# Checks if $entry is a duplicate
sub isDuplicate ( $deduplicate, $entry ) {
    if ( $entry->{imdbID} && $deduplicate->{ $entry->{imdbID} } ) {
        return 1;
    }
    if ( $entry->{tmdbID} && $deduplicate->{ $entry->{tmdbID} } ) {
        return 1;
    }
    my $identifier = $entry->{title} . '//-//' . $entry->{year};
    return $deduplicate->{$identifier};
}

# Stores $entry for future use by isDuplicate
sub storeDeduplication ( $deduplicate, $entry ) {
    if ( $entry->{imdbID} ) {
        $deduplicate->{ $entry->{imdbID} } = 1;
    }
    if ( $entry->{tmdbID} ) {
        $deduplicate->{ $entry->{tmdbID} } = 1;
    }
    my $identifier = $entry->{title} . '//-//' . $entry->{year};
    $deduplicate->{$identifier} = 1;
}

# Converts rotcelloc JSON data to Letterboxd-compatible CSV
sub jsonToCSV ( $json, %filters ) {
    my @lines;
    my %deduplicate;
    my $duplicates = 0;
    push( @lines, [ 'Title', 'Year', 'tmdbID', 'imdbID', 'Review' ] );
    foreach my $entry ( @{ $json->{data} } ) {
        if ( isDuplicate( \%deduplicate, $entry ) ) {
            $duplicates++;
        }
        if ( defined $filters{watched} ) {
            if ( $filters{watched} && !$entry->{watched} ) {
                next;
            }
            if ( !$filters{watched} && $entry->{watched} ) {
                next;
            }
        }
        my $note = '';
        if ( defined $entry->{format} && length( @{ $entry->{format} } ) ) {
            $note = join( '+', @{ $entry->{format} } );
        }
        if ( defined $filters{genre} ) {
            my $match = 0;
            foreach my $genre ( @{ $filters{genre} } ) {
                foreach my $movieGenre ( @{ $entry->{genres} } ) {
                    if ( lc($movieGenre) eq $genre ) {
                        $match = 1;
                    }
                }
            }
            if ( !$match ) {
                next;
            }
        }
        if ( defined $filters{format} ) {
            my $match = 0;
            foreach my $format ( @{ $filters{format} } ) {
                foreach my $movieFormat ( @{ $entry->{format} } ) {
                    if ( lc($movieFormat) eq $format ) {
                        $match = 1;
                    }
                }
            }
            if ( !$match ) {
                next;
            }
        }
        if ( defined $filters{addedLast} ) {
            my $match     = 0;
            my $addedTime = int( $entry->{added} );
            if ( $addedTime < $filters{addedLast} ) {
                next;
            }
        }
        if ( !defined( $entry->{tmdbID} ) && !defined( $entry->{imdbID} ) ) {
            if ( !defined( $entry->{year} ) || !length( $entry->{year} ) ) {
                printv( 'Skipped entry for ' . $entry->{title} . ': no year' );
                next;
            }
            if ( $filters{requireID} ) {
                next;
            }
        }
        elsif ( $filters{onlyIncomplete} ) {
            next;
        }
        if ( !defined( $entry->{tmdbID} ) ) {
            if ( $filters{requireIMDB} ) {
                next;
            }
        }
        storeDeduplication( \%deduplicate, $entry );
        push(
            @lines,
            [
                $entry->{title},  $entry->{sortYear}, $entry->{tmdbID},
                $entry->{imdbID}, $note
            ]
        );
    }
    printv("Skipped $duplicates duplicates") if $duplicates > 0;
    return Mojo::CSV->new->text( \@lines );
}

# Outputs help
sub help () {
    say 'Usage: rotcelloc-to-letterboxd.pl [OPTIONS] URL|PATH COLLECTION_NAME';
    say '';
    say 'URL|PATH is either the URL to the rotcelloc site, or the path';
    say 'to a local copy of a built rotcelloc site';
    say '';
    say 'COLLECTION_NAME is the name defined in rotcelloc.config.cson';
    say '';
    say 'Options for filtering the output';
    say '   --require-metadata-id     Require an IMDB or TMDB ID for entries';
    say '   --require-imdb-id         Require an IMDB ID for entries';
    say '   --only-incomplete         Exclude entries with a metadata ID.';
    say
'                             This is only useful for finding movies that';
    say '                             you need to fix.';
    say '   --only-watched            Only output watched movies';
    say '   --only-not-watched        Only output not watched movies';
    say
      '   --genre STRING            Only output movies in the genre specified';
    say
'                             May be specified multiple times, in which case';
    say
'                             movies matching any of the genres will be output';
    say
      '   --format STRING           Only output movies in the format specified';
    say
'                             May be specified multiple times, in which case';
    say
'                             movies matching any of the formats will be output';
    say '   --added-last-weeks NO     Only output entries modified the last NO';
    say
'                             weeks (only one --added-last-* may be used)';
    say '   --added-last-days  NO     Only output entries modified the last NO';
    say
      '                             days (only one --added-last-* may be used)';

    say '';
    say 'Other options';
    say '   --help                    Show this help screen';
    say
'   --output-file             Which file to output to. If not specified then';
    say '                             letterboxd.csv will be used';
    say '   --overwrite               Overwrite output-file if it exists';
}

# Main function
sub main () {
    my %filters;
    my $outFile   = 'letterboxd.csv';
    my $overwrite = 0;
    GetOptions(
        'require-metadata-id' => sub {
            $filters{requireID} = 1;
        },
        'require-imdb-id' => sub {
            $filters{requireIMDB} = 1;
        },
        'only-incomplete' => sub {
            $filters{onlyIncomplete} = 1;
        },
        'only-watched' => sub {
            $filters{watched} = 1;
        },
        'only-not-watched' => sub {
            $filters{watched} = 0;
        },
        'genre=s' => sub {
            shift;
            my $genre = shift;
            $filters{genre} //= [];
            push( @{ $filters{genre} }, lc($genre) );
        },
        'format=s' => sub {
            shift;
            my $format = shift;
            $filters{format} //= [];
            push( @{ $filters{format} }, lc($format) );
        },
        'help' => sub {
            help();
            exit(0);
        },
        'overwrite'          => \$overwrite,
        'output-file=s'      => \$outFile,
        'added-last-weeks=i' => sub {
            shift;
            my $timeString = shift;
            $filters{addedLast} = time - ( ( $timeString * 7 ) * 86400 );
        },
        'added-last-days=i' => sub {
            shift;
            my $timeString = shift;
            $filters{addedLast} = time - ( $timeString * 86400 );
        },
    ) or die("See --help\n");
    my $path       = shift(@ARGV);
    my $collection = shift(@ARGV);
    if (   !defined($path)
        || !length($path)
        || !defined($collection)
        || !length($collection) )
    {
        die(
"Usage: rotcelloc-to-letterboxd.pl [OPTIONS] URL|PATH COLLECTION_NAME\nSee --help for more information.\n"
        );
    }
    if ( io($outFile)->exists && !$overwrite ) {
        die("$outFile: already exists\n");
    }
    my $json = getParsedData( $path, $collection );
    my $csv  = jsonToCSV( $json, %filters );
    io($outFile)->write($csv);
    say "Wrote $outFile";
}

main();
