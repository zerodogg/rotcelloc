/**
 * Use this on https://steamcommunity.com/id/[[YOUR ACCOUNT NAME]]/games/?tab=all
 */
((document) => {
    let list = [
        ["Title","Steam ID","Platform"]
    ];
    let games = JSON.parse(document.querySelectorAll('#gameslist_config')[0].getAttribute('data-profile-gameslist'))
    for(const game of games.rgGames)
    {
        if (
            // Ignore DLC and season passes
            /\s(DLC|[Ss]eason\s+[Pp]ass)/.test(game.title) ||
            // Other DLC matches
            /\s((Map|Texture)\s*Pack)(\s*\d*)?$/.test(game.title) ||
            // Ignore games returned from Steam with completely wrong
            // names
            /^\s*(Limited\s*Edition)\s*$/i.test(game.title)
        )
        {
            continue;
        }
        list.push([
            game.name.replace(/;/g,"\\;"),
            game.appid,
            "Steam",
        ]);
    }
    let CSV = '';
    for (const line of list)
    {
        CSV += line.join(';')+"\n";
    }
    const a = document.createElement("a");
    a.setAttribute("download", "steam-games.csv");
    a.setAttribute(
        "href",
        "data:text/csv;charset=utf-8," + encodeURIComponent(CSV)
    );
    document.querySelector("body").append(a);
    a.click();
})(document);
