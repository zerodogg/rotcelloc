import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

export default defineConfig(() => {
    return {
        root: "webapp",
        build: {
            outDir: path.resolve(__dirname, "build"),
            emptyOutDir: true,
        },
        plugins: [react()],
        resolve: {
            alias: {
                "~bootstrap": path.resolve(__dirname, "node_modules/bootstrap"),
            },
        },
    };
});
