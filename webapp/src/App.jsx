/*
 * rotcelloc base react UI component
 *
 * Part of rotcelloc - the hacker's movie, tv-series and game collection
 * manager
 *
 * Copyright (C) Eskild Hustvedt 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import "./App.scss";
import { useState, useMemo } from "react";
import React from "react";
import { getCollectionInstance, getRotcelloc } from "./rotcellocData.js";
import {
    bumpVisibleItems,
    displayAsTable,
    genreSearchType,
    searchInPlot,
    searchString,
    showOnlyFormats,
    showOnlyGenres,
    showOnlyGroup,
    showOnlyPlatforms,
    showOnlyWatched,
    sortOrder,
    visibleItems,
} from "./State.js";
import { useRecoilState, useSetRecoilState, useResetRecoilState } from "recoil";
import { dumbHistory } from "./dumbHistory.js";
import {
    Alert,
    Collapse,
    Container,
    Nav,
    NavItem,
    NavLink,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Row,
    Spinner,
    Table,
} from "reactstrap";
import { CollectionItem, CollectionItemTableRow } from "./CollectionItemUI.jsx";
import { FilterMenu } from "./FilterMenuUI.jsx";

function App() {
    const r = getRotcelloc();
    const [initialized, setInitialized] = useState(false);
    const [, setPath] = useState(window.location.pathname);
    const increaseVisibleItemsBy = useSetRecoilState(bumpVisibleItems);

    // Code that is run once and only once
    useMemo(() => {
        r.onInitialized(() => {
            // Needs to be done in a timeout so that react doesn't get confused
            // by state being modified during a render
            setTimeout(() => {
                setInitialized(true);
            }, 1);
        });
        const body = document.querySelector("body");
        const html = document.querySelector("html");
        window.addEventListener("scroll", (_) => {
            if (html.scrollTop >= body.getBoundingClientRect().height - 2000) {
                increaseVisibleItemsBy(15);
            }
        });
        dumbHistory.listen(() => {
            setTimeout(() => {
                setPath(window.location.pathname);
            }, 1);
        });
    }, []);

    if (initialized) {
        let pathname = window.location.pathname;
        if (pathname === "/" || pathname === undefined) {
            pathname = "/" + r.getDefaultCollection();
        }
        return (
            <div>
                <MainMenu path={pathname} />
                <Container>
                    <RotcellocRenderPath path={pathname} />
                </Container>
            </div>
        );
    } else {
        return (
            <div>
                <MainMenu placeholder />
                <Container>
                    <RotcellocIsLoading />
                </Container>
            </div>
        );
    }
}

function RotcellocIsLoading() {
    let text = "Loading...";
    const r = getRotcelloc();
    if (r.ready) {
        text = r.translate("Loading collection...");
    }
    return <Spinner>{text}</Spinner>;
}

function RotcellocRenderPath({ path }) {
    const r = getRotcelloc();
    const [previousPath, setPreviousPath] = useState(null);
    const setSortOrder = useSetRecoilState(sortOrder);
    const resetGenreSearchType = useResetRecoilState(genreSearchType);
    const resetSearchInPlot = useResetRecoilState(searchInPlot);
    const resetSearchString = useResetRecoilState(searchString);
    const resetShowOnlyFormats = useResetRecoilState(showOnlyFormats);
    const resetShowOnlyGenres = useResetRecoilState(showOnlyGenres);
    const resetShowOnlyGroup = useResetRecoilState(showOnlyGroup);
    const resetShowOnlyPlatforms = useResetRecoilState(showOnlyPlatforms);
    const resetShowOnlyWatched = useResetRecoilState(showOnlyWatched);
    const [, setVisibleItems] = useRecoilState(visibleItems);

    if (!r.isValidPath(path)) {
        return (
            <Alert color="danger">
                404: Collection not found for path &quot;{path}&quot;
            </Alert>
        );
    }

    if (previousPath !== path) {
        r.initializeCollection(
            r.parseCollectionFromPath(path),
            (collection) => {
                setTimeout(() => {
                    setPreviousPath(path);
                    setVisibleItems(r.maxEntriesPerRenderedPage);
                    resetGenreSearchType();
                    resetSearchInPlot();
                    resetSearchString();
                    resetShowOnlyFormats();
                    resetShowOnlyGenres();
                    resetShowOnlyGroup();
                    resetShowOnlyPlatforms();
                    resetShowOnlyWatched();
                    setSortOrder(collection.workingConfig.defaultSort);
                }, 1);
            },
        );
        return <RotcellocIsLoading />;
    }

    return <RotcellocBase />;
}

// eslint-disable-next-line
function RotcellocBase() {
    return (
        <div>
            <FilterMenu />
            <RenderCollectionFromSearch />
        </div>
    );
}

function RenderCollectionFromSearch() {
    const c = getCollectionInstance();
    const [currentSearchString] = useRecoilState(searchString);
    const [currentSortOrder] = useRecoilState(sortOrder);
    const [currentSearchInPlot] = useRecoilState(searchInPlot);
    const [currentShowOnlyGroup] = useRecoilState(showOnlyGroup);
    const [currentShowOnlyWatched] = useRecoilState(showOnlyWatched);
    const [currentShowOnlyGenres] = useRecoilState(showOnlyGenres);
    const [currentShowOnlyPlatforms] = useRecoilState(showOnlyPlatforms);
    const [currentShowOnlyFormats] = useRecoilState(showOnlyFormats);
    const [currentGenreSearchType] = useRecoilState(genreSearchType);
    const [currentDisplayAsTable] = useRecoilState(displayAsTable);
    const searchQuery = {};
    if (currentSearchString) {
        searchQuery.text = currentSearchString.toLowerCase();
    }
    if (currentSortOrder) {
        searchQuery.order = currentSortOrder;
    }
    if (currentShowOnlyWatched === true) {
        searchQuery.watchedSearch = true;
    }
    if (currentSearchInPlot === true) {
        searchQuery.plotSearch = true;
    }
    if (currentShowOnlyGroup !== "") {
        searchQuery.group = currentShowOnlyGroup;
    }
    if (currentShowOnlyPlatforms.length) {
        searchQuery.platform = currentShowOnlyPlatforms;
    }
    if (currentShowOnlyGenres.length) {
        searchQuery.genres = currentShowOnlyGenres;
        searchQuery.genreSearchType = currentGenreSearchType;
    }
    if (currentShowOnlyFormats.length) {
        searchQuery.formats = currentShowOnlyFormats;
    }
    if (currentDisplayAsTable === true) {
        return (
            <div>
                <RenderCollectionItemsTable
                    results={c.performSearch(searchQuery)}
                />
            </div>
        );
    } else {
        return (
            <div>
                <RenderCollectionItems results={c.performSearch(searchQuery)} />
            </div>
        );
    }
}

function RenderCollectionItems({ results }) {
    const c = getCollectionInstance();
    setPageTitle(c.pagetype + " (" + results.length + ")");
    if (results.length === 0) {
        return <div>No results</div>;
    }
    const [itemsToDisplay] = useRecoilState(visibleItems);

    const items = [];
    let currentRow = [];
    let rowItemNo = 0;
    const pushCurrentRow = () => {
        if (currentRow.length > 0) {
            items.push(
                <CollectionRow key={"row-" + items.length}>
                    {currentRow}
                </CollectionRow>,
            );
        }
        currentRow = [];
    };

    for (let item = 0; item < itemsToDisplay; item++) {
        if (results[item] === undefined) {
            break;
        }
        const itemContent = results[item];
        rowItemNo++;
        currentRow.push(
            <CollectionItem
                key={"itemno-" + item + itemContent.title}
                item={results[item]}
            />,
        );
        if (rowItemNo >= 4) {
            pushCurrentRow();
            rowItemNo = 0;
        }
    }

    pushCurrentRow();

    return items;
}

function RenderCollectionItemsTable({ results }) {
    const c = getCollectionInstance();
    const r = getRotcelloc();
    const setSortOrder = useSetRecoilState(sortOrder);
    setPageTitle(c.pagetype + " (" + results.length + ")");
    if (results.length === 0) {
        return <div>No results</div>;
    }
    let extra = null;
    const first = r.translate("Title");
    let second = r.translate("Format");
    let third = r.translate("Genre");

    if (c.workingConfig.type === "books") {
        second = r.translate("Author");
    } else if (
        c.workingConfig.type === "manga" ||
        c.workingConfig.type === "comicBooks"
    ) {
        second = r.translate("Volumes");
    } else if (c.workingConfig.type === "games") {
        second = r.translate("Platform");
        third = r.translate("Genre");
    } else if (c.workingConfig.type === "series") {
        second = r.translate("Seasons");
        third = r.translate("Format");
        extra = <th>{r.translate("Genre")}</th>;
    }

    let [itemsToDisplay] = useRecoilState(visibleItems);
    itemsToDisplay += 100;

    const items = [];

    for (let item = 0; item < itemsToDisplay; item++) {
        if (results[item] === undefined) {
            break;
        }
        const itemContent = results[item];
        items.push(
            <CollectionItemTableRow
                key={"itemno-" + item + itemContent.title}
                item={results[item]}
            />,
        );
    }

    return (
        <Table hover>
            <thead>
                <tr>
                    <th
                        style={{ cursor: "pointer" }}
                        onClick={() => setSortOrder("alpha")}
                    >
                        {first}
                    </th>
                    <th>{second}</th>
                    <th>{third}</th>
                    {extra}
                </tr>
            </thead>
            <tbody>{items}</tbody>
        </Table>
    );
}

function CollectionRow({ children }) {
    return <Row>{children}</Row>;
}

function MainMenu({ path, placeholder }) {
    const NavItems = [];
    const [isOpen, setIsOpen] = useState(false);
    let menuSiteTitle = "";
    if (placeholder) {
        NavItems.push(
            <NavItem key="placeholder">
                <NavLink>&nbsp;</NavLink>
            </NavItem>,
        );
    } else {
        const rotcelloc = getRotcelloc();
        const currentCollection = rotcelloc.getCollectionNameFromPath(path);
        menuSiteTitle = rotcelloc.index().config.menuSiteTitle;
        for (const itemName of Object.keys(
            rotcelloc.index().config.collections,
        )) {
            let active = false;
            if (currentCollection === itemName) {
                active = true;
            }
            NavItems.push(
                <NavItem key={"menu-" + itemName}>
                    <NavLink
                        active={active}
                        href="#"
                        onClick={() => {
                            dumbHistory.pushHistory(
                                "/" + rotcelloc.getPathFromCategory(itemName),
                            );
                            setIsOpen(false);
                        }}
                    >
                        {itemName}
                    </NavLink>
                </NavItem>,
            );
        }
    }
    return (
        <Navbar color="light" light={true} container="fluid" expand="md">
            <NavbarBrand className="d-md-none">{menuSiteTitle}</NavbarBrand>
            <NavbarToggler onClick={() => setIsOpen(!isOpen)} />
            <Collapse isOpen={isOpen} navbar>
                <Nav navbar className="nav-underline me-auto">
                    {NavItems}
                </Nav>
            </Collapse>
            <NavbarBrand className="d-none d-md-block">
                {menuSiteTitle}
            </NavbarBrand>
        </Navbar>
    );
}

function setPageTitle(title) {
    document.querySelector("title").innerText = title + " -  rotcelloc";
}

export default App;
