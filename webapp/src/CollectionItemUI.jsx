/*
 * rotcelloc collection item UI
 *
 * Part of rotcelloc - the hacker's movie, tv-series and game collection
 * manager
 *
 * Copyright (C) Eskild Hustvedt 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import "./App.scss";
import { useState } from "react";
import React from "react";
import { getCollectionInstance, getRotcelloc } from "./rotcellocData.js";
import {
    searchString,
    showOnlyGroup,
    toggleGroupInOnlyFormats,
    toggleGroupInOnlyGenres,
    showGroupAboveTheFold,
} from "./State.js";
import { useRecoilState, useSetRecoilState } from "recoil";
import { Badge, Card, Col } from "reactstrap";

function CollectionItemTableRow({ item }) {
    const c = getCollectionInstance();
    let year = null;
    if (item.year !== undefined && item.year !== null && item.year !== "") {
        year = "(" + item.year + ")";
    }
    let second;
    let third;
    let extra = null;
    const conditionalJoinList = (arr) => {
        if (arr !== undefined && arr !== null) {
            if (Array.isArray(arr)) {
                return arr.join(", ");
            }
            return arr;
        }
        return null;
    };

    if (item.type === "movies") {
        second = conditionalJoinList(item.format);
        third = conditionalJoinList(item.genres);
    } else if (item.type === "series") {
        second = item.seasons;
        third = conditionalJoinList(item.format);
        extra = <td>{conditionalJoinList(item.genres)}</td>;
    } else if (item.type === "game") {
        if (
            item.platform !== null &&
            item.platform !== undefined &&
            item.platform !== "" &&
            item.platform.length > 0
        ) {
            second = conditionalJoinList(item.platform);
        } else if (
            item.format !== null &&
            item.format !== undefined &&
            item.format !== "" &&
            item.format.length > 0
        ) {
            second = conditionalJoinList(item.format);
        } else {
            if (Array.isArray(item.bSourceList)) {
                second = c.workingMeta.sourceToNameMap[item.bSourceList[0]];
            } else {
                second = c.workingMeta.sourceToNameMap[item.bSourceList];
            }
        }
        third = conditionalJoinList(item.genres);
    } else if (item.type === "books") {
        second = item.author;
        third = conditionalJoinList(item.genres);
    } else if (item.type === "manga" || item.type === "comicBooks") {
        second = item.volumes;
        third = conditionalJoinList(item.genres);
    } else {
        second = "Unhandled item.type=" + item.type;
    }

    return (
        <tr>
            <td>
                {item.title} {year}
            </td>
            <td>{second}</td>
            <td>{third}</td>
            {extra}
        </tr>
    );
}

function CollectionItem({ item }) {
    const r = getRotcelloc();
    const c = getCollectionInstance();
    const toggleCurrentGenres = useSetRecoilState(toggleGroupInOnlyGenres);
    const [, setShowOnlyGroup] = useRecoilState(showOnlyGroup);
    const [currentShowGroupAboveTheFold] = useRecoilState(
        showGroupAboveTheFold,
    );
    let origTitle;
    if (item.origTitle && item.origTitle !== item.title) {
        origTitle = item.origTitle;
    }
    let year = null;
    if (item.year !== undefined && item.year !== null && item.year !== "") {
        const setSearchString = useSetRecoilState(searchString);
        year = (
            <>
                &#32;(
                <LinkLookalike onClick={() => setSearchString(item.year)}>
                    {item.year}
                </LinkLookalike>
                )
            </>
        );
    }
    return (
        <Col md="3" className="pt-2" data-searchScore={item.searchScore}>
            <CollectionItemCover item={item} />
            <CollectionItemPills item={item} />
            <h3>
                {item.title}
                {year}
            </h3>
            <CollectionItemMetadataLine
                title={r.translate("Group")}
                value={c.workingMeta.sourceToNameMap[item.bSourceList]}
                cssClass=""
                onClick={() => setShowOnlyGroup(item.bSourceList)}
                invisible={currentShowGroupAboveTheFold === false}
            />
            <CollectionItemMetadataLine value={item.author} cssClass="author" />
            <CollectionItemMetadataLine
                title={r.translate("Seasons")}
                value={item.seasons}
                cssClass="seasons"
            />
            <CollectionItemMetadataLine
                title={r.translate("Volumes")}
                value={item.volumes}
                cssClass="volumes"
            />
            <CollectionItemMetadataLine
                title={r.translate("Original title")}
                value={origTitle}
                cssClass="original-title"
            />
            <CollectionItemMetadataLine
                title={r.translate("Disney classics no.")}
                value={item.disneyClassicNo}
                cssClass="disney-classics-no"
            />
            <CollectionItemMetadataLine
                title={r.translate("Genre")}
                value={item.genres}
                cssClass="genre"
                onClick={(genre) => toggleCurrentGenres(genre)}
            />
            <ShowMore>
                <CollectionItemAdditionalMetadata item={item} />
            </ShowMore>
        </Col>
    );
}

function CollectionItemPills({ item }) {
    let entries = null;
    if (item.type === "series" || item.type === "movies") {
        entries = item.format;
    }
    if (entries === null || entries === undefined || entries.length === 0) {
        return null;
    }
    const toggleCurrentFormats = useSetRecoilState(toggleGroupInOnlyFormats);
    const pills = [];
    let pillNo = 0;
    for (const entry of entries.sort()) {
        pills.push(
            <Badge
                key={pillNo++}
                className="text-dark mr-1 linkLike"
                onClick={() => toggleCurrentFormats(entry)}
                color="light"
            >
                {entry}
            </Badge>,
        );
    }
    return <div>{pills}</div>;
}

function CollectionItemAdditionalMetadata({ item }) {
    const r = getRotcelloc();
    const c = getCollectionInstance();
    const entries = [];
    const [, setSearchString] = useRecoilState(searchString);
    if (item.altTitle && item.altTitle !== item.title) {
        entries.push(
            <CollectionItemMetadataLine
                title={r.translate("Alternative title")}
                value={item.altTitle}
                cssClass="altTitle"
                key="altTitle"
                searchable={false}
            />,
        );
    }
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Runtime")}
            value={item.runtime}
            cssClass="runtime"
            key="runtime"
            searchable={false}
        />,
    );
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Platform")}
            value={item.platform}
            cssClass="platform"
            key="platform"
            searchable={true}
        />,
    );
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Developer")}
            value={item.developer}
            cssClass="developer"
            key="developer"
            searchable={true}
            onClick={(searchBy) => {
                setSearchString(
                    searchBy.replace(/\([^)]+\)/g, "").replace(/\s+$/, ""),
                );
            }}
        />,
    );
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Actors")}
            value={item.actors}
            cssClass="actors"
            key="actors"
            searchable={true}
            onClick={(searchBy) => {
                setSearchString(
                    searchBy.replace(/\([^)]+\)/g, "").replace(/\s+$/, ""),
                );
            }}
        />,
    );
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Director")}
            value={item.director}
            cssClass="director"
            key="director"
            searchable={true}
            onClick={(searchBy) => {
                setSearchString(
                    searchBy.replace(/\([^)]+\)/g, "").replace(/\s+$/, ""),
                );
            }}
        />,
    );
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Writer")}
            value={item.writer}
            cssClass="writer"
            key="writer"
            searchable={true}
            onClick={(searchBy) => {
                setSearchString(
                    searchBy.replace(/\([^)]+\)/g, "").replace(/\s+$/, ""),
                );
            }}
        />,
    );
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Volumes total")}
            value={item.publishedVolumes}
            cssClass="publishedVolumes"
            key="publishedVolumes"
        />,
    );
    const [, setShowOnlyGroup] = useRecoilState(showOnlyGroup);
    const [currentShowGroupAboveTheFold] = useRecoilState(
        showGroupAboveTheFold,
    );
    const genericEntriesOrder = [
            "language",
            "rating",
            "bSourceList",
            "metascore",
            "imdbRating",
            "tmdbRating",
            "isbn",
            "publisher",
        ],
        genericEntries = {
            rating: {
                searchable: false,
                label: r.translate("Custom rating"),
                renderer: (val) => {
                    return val + "/6";
                },
            },
            metascore: {
                searchable: false,
                label: r.translate("Metascore"),
                renderer: (val) => {
                    return val + "/100";
                },
            },
            bSourceList: {
                searchable: false,
                label: r.translate("Group"),
                onClick: (value) => setShowOnlyGroup(value),
                renderer: (val) => {
                    return c.workingMeta.sourceToNameMap[val];
                },
                invisible: currentShowGroupAboveTheFold === true,
            },
            tmdbRating: {
                searchable: false,
                label: r.translate("TMDB rating"),
                renderer: (val, entryData) => {
                    return (
                        entryData.tmdbRating +
                        "/10 (" +
                        entryData.tmdbVotes +
                        " " +
                        r.translate("votes") +
                        ")"
                    );
                },
            },
            imdbRating: {
                searchable: false,
                label: r.translate("IMDB rating"),
                renderer: (val, entryData) => {
                    return (
                        entryData.imdbRating +
                        "/10 (" +
                        entryData.imdbVotes +
                        " " +
                        r.translate("votes") +
                        ")"
                    );
                },
            },
            language: {
                searchable: true,
                label: r.translate("Language"),
            },
            publisher: {
                searchable: true,
                label: r.translate("Publisher"),
            },
            isbn: {
                searchable: false,
                label: r.translate("ISBN"),
            },
        };

    for (
        let genericEntryCurrI = 0;
        genericEntryCurrI < genericEntriesOrder.length;
        genericEntryCurrI++
    ) {
        const genericEntryCurr = genericEntriesOrder[genericEntryCurrI];
        if (item[genericEntryCurr]) {
            const renderRules = genericEntries[genericEntryCurr];
            const value = item[genericEntryCurr];
            let renderer;
            if (renderRules.renderer) {
                renderer = (val) => renderRules.renderer(val, item);
            }
            entries.push(
                <CollectionItemMetadataLine
                    title={renderRules.label}
                    value={value}
                    cssClass={genericEntryCurr}
                    key={"genericNo-" + genericEntryCurrI}
                    searchable={renderRules.searchable}
                    onClick={renderRules.onClick}
                    renderValueWith={renderer}
                />,
            );
        }
    }

    //    this.renderSingleMetaitem($target,r.translate('Links'),this.getLinks(),'links',false,true);
    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Note")}
            value={item.note}
            cssClass="note"
            key="note"
            searchable={true}
        />,
    );

    entries.push(
        <CollectionItemMetadataLine
            title={r.translate("Date added")}
            value={item.addedRaw}
            cssClass="addedRaw"
            key="addedRaw"
            searchable={false}
        />,
    );

    const links = <CollectionItemLinks item={item} />;
    entries.push(
        <CollectionItemMetadataComponent
            title={r.translate("Links")}
            renderParts={links}
        />,
    );

    if (item.plot) {
        entries.push(
            <div className="pt-1 fst-italic" key="plot">
                {item.plot}
            </div>,
        );
    }

    return <>{entries}</>;

    // this.renderSingleMetaitem($target,null,item.plot,'plot');
}

function CollectionItemMetadataLine({
    title,
    value,
    callbackValue,
    cssClass,
    onClick,
    renderValueWith,
    invisible,
}) {
    const renderParts = [];
    if (value === undefined || invisible === true) {
        return null;
    }
    if (cssClass === undefined) {
        if (title === undefined) {
            cssClass = "metadata-entry";
        } else {
            cssClass = title.toLowerCase().replace(/\s+/g, "-");
        }
    }
    if (!Array.isArray(value)) {
        if (onClick) {
            value = value.split(/,\s+/);
        } else {
            value = [value];
        }
    }
    for (let n = 0; n < value.length; n++) {
        const component = value[n];
        if (n > 0) {
            renderParts.push(<span key={"commaNo-" + n}>, </span>);
        }
        if (onClick !== undefined) {
            let clickValue = component;
            let renderLabel = component;
            if (callbackValue !== undefined) {
                clickValue = callbackValue;
            }
            if (renderValueWith !== undefined) {
                renderLabel = renderValueWith(component);
            }
            renderParts.push(
                <LinkLookalike
                    key={"contentNo-" + n}
                    onClick={() => onClick(clickValue)}
                >
                    {renderLabel}
                </LinkLookalike>,
            );
        } else {
            renderParts.push(<span key={"contentNo-" + n}>{component}</span>);
        }
    }
    return (
        <CollectionItemMetadataComponent
            title={title}
            renderParts={renderParts}
        />
    );
}

function CollectionItemMetadataComponent({ title, renderParts }) {
    if (title !== undefined && title !== null && title !== "") {
        return (
            <div>
                <span className="meta-label">{title}:</span>
                {renderParts}
            </div>
        );
    } else {
        return <div>{renderParts}</div>;
    }
}

function CollectionItemCover({ item }) {
    const [didError, setError] = useState(false);
    if (!item.poster || didError) {
        return <CollectionItemPlaceholderCover item={item} />;
    }
    return (
        <div className="poster">
            <img
                src={"/images/" + item.poster}
                className="img-fluid rounded"
                alt=""
                onError={() => setError(true)}
            />
        </div>
    );
}

function CollectionItemPlaceholderCover({ item }) {
    const more = [];
    if (item.author) {
        more.push(<h4 key="headerAuthor">{item.author}</h4>);
    } else if (item.seasons) {
        more.push(<h4 key="headerSeasons">{item.seasons}</h4>);
    }
    if (item.publisher) {
        more.push(<i key="publisher">{item.publisher}</i>);
    }
    return (
        <Card
            color="light"
            className="poster rounded poster-generated text-center"
        >
            <h1>{item.title}</h1>
            {more}
        </Card>
    );
}

function CollectionItemLinks({ item }) {
    const types = {
        series: CollectionItemLinksMovieTV,
        movies: CollectionItemLinksMovieTV,
        game: CollectionItemLinksGame,
        books: CollectionItemLinksBook,
        manga: CollectionItemLinksManga,
        comicBooks: CollectionItemLinksComicBooks,
        generic: CollectionItemLinksGeneric,
    };
    const Variant = types[item.type];
    if (Variant !== undefined) {
        const title = item.origTitle ? item.origTitle : item.title;
        return <Variant item={item} title={title} />;
    }
    throw 'CollectionItemLinks: unknown type "' + item.type + '"';
}

function getYouTubeTrailerSearchURL(item) {
    let trailerSearch = item.normalizedTitle;
    if (item.year) {
        trailerSearch += " " + item.year.replace(/\D.*/g, "");
    }
    if (item.type === "game") {
        trailerSearch += " game";
    } else if (item.type === "series") {
        trailerSearch += " (tv OR series)";
    } else if (item.type === "movies") {
        trailerSearch += " (movie OR theatrical OR film)";
    }
    trailerSearch += ' "trailer"';
    return (
        "https://www.youtube.com/results?search_query=" +
        encodeURIComponent(trailerSearch)
    );
}

function CollectionItemLinksMovieTV({ item, title }) {
    let letterboxdURL;
    if (item.imdbID) {
        letterboxdURL = "https://letterboxd.com/imdb/" + item.imdbID;
    } else if (item.tmdbID) {
        letterboxdURL = "https://letterboxd.com/tmdb/" + item.tmdbID;
    } else {
        letterboxdURL =
            "https://letterboxd.com/search/films/" + encodeURIComponent(title);
    }
    let imdbURL;
    if (item.imdbID) {
        imdbURL = "http://www.imdb.com/title/" + item.imdbID;
    } else {
        imdbURL =
            "http://www.imdb.com/find?s=all&amp;s=tt&amp;q=" +
            encodeURIComponent(item.origTitle ? item.origTitle : item.title) +
            "&amp;ttype=" +
            (item.type === "series" ? "tv" : "ft");
    }
    let tmdbURL;
    if (item.tmdbID) {
        tmdbURL =
            "https://www.themoviedb.org/" +
            (item.type === "series" ? "tv" : "movie") +
            "/" +
            item.tmdbID;
    } else {
        tmdbURL =
            "https://www.themoviedb.org/search/" +
            (item.type === "series" ? "tv" : "movie") +
            "?query=" +
            encodeURIComponent(item.origTitle ? item.origTitle : item.title);
    }
    const links = [
        { Letterboxd: letterboxdURL },
        { IMDB: imdbURL },
        { TheMovieDB: tmdbURL },
        { "Trailer (YouTube)": getYouTubeTrailerSearchURL(item) },
    ];
    if (item.contentType && item.contentType === "anime") {
        links.unshift({
            AnimeNFO:
                "https://duckduckgo.com/?q=" +
                encodeURIComponent(
                    (item.origTitle ? item.origTitle : item.title) +
                        " site:animenfo.com",
                ),
        });
    }

    return <CollectionItemLinksElement links={links} />;
}
function CollectionItemLinksBook({ item }) {
    let openLibraryLink = item.openliblink;
    if (item.openliblink === undefined) {
        openLibraryLink = "https://openlibrary.org/search?q=" + item.isbn;
    }
    const links = [
        { OpenLibrary: openLibraryLink },
        {
            Goodreads:
                "https://www.goodreads.com/search?utf8=%E2%9C%93&query=" +
                item.isbn,
        },
        {
            LibraryThing:
                "https://www.librarything.com/search.php?search=" +
                item.isbn +
                "&searchtype=38&searchtype=38&sortchoice=0",
        },
    ];
    return <CollectionItemLinksElement links={links} />;
}
function CollectionItemLinksGame({ item }) {
    let theGamesDBURL;
    if (item.tgdbID) {
        theGamesDBURL = '"http://thegamesdb.net/game.php?id=' + item.tgdbID;
    } else {
        theGamesDBURL =
            "http://thegamesdb.net/search.php?name=" +
            encodeURIComponent(item.title);
    }
    const links = [
        {
            MobyGames:
                "https://www.mobygames.com/search/quick?q=" +
                encodeURIComponent(item.title),
        },
        { TheGamesDB: theGamesDBURL },
    ];
    if (item.steamID) {
        links.push({
            Steam:
                "http://store.steampowered.com/app/" +
                encodeURIComponent(item.steamID),
        });
    } else if (
        item.platform.indexOf("PC") ||
        item.platform.indexOf("Linux") ||
        item.platform.indexOf("Windows") ||
        item.platform.indexOf("Mac")
    ) {
        links.push({
            Steam:
                "https://store.steampowered.com/search/?term=" +
                encodeURIComponent(item.title),
        });
    }
    links.push({ "Trailer (YouTube)": getYouTubeTrailerSearchURL(item) });
    return <CollectionItemLinksElement links={links} />;
}
function CollectionItemLinksComicBooks({ item }) {
    let comicVineURL =
        "https://comicvine.gamespot.com/search/?i=&q=" +
        encodeURIComponent(item.title);
    if (item.comicVineDetailURL !== undefined) {
        comicVineURL = item.comicVineDetailURL;
    }
    const links = [{ ComicVine: comicVineURL }];
    return <CollectionItemLinksElement links={links} />;
}
function CollectionItemLinksManga({ item }) {
    let anilistURL =
        "https://anilist.co/search/manga?search=" +
        encodeURIComponent(item.title);
    if (item.anilistID !== undefined) {
        anilistURL = "https://anilist.co/manga/" + item.anilistID;
    }
    const links = [{ AniList: anilistURL }];
    return <CollectionItemLinksElement links={links} />;
}
function CollectionItemLinksGeneric({ item }) {
    const links = [
        {
            DuckDuckGo:
                "https://duckduckgo.com/?q=" + encodeURIComponent(item.title),
        },
    ];
    return <CollectionItemLinksElement links={links} />;
}

function CollectionItemLinksElement({ links }) {
    const renderElements = [];
    let linkNumber = 0;
    for (const link of links) {
        linkNumber++;
        const name = Object.keys(link)[0];
        const URL = link[name];
        renderElements.push(
            <>
                <a target="_blank" rel="noreferrer" href={URL}>
                    {name}
                </a>
                {linkNumber < links.length ? ", " : ""}
            </>,
        );
    }
    return <>{renderElements}</>;
}

function ShowMore({ children }) {
    const [open, setOpen] = useState(false);
    const r = getRotcelloc();
    if (open) {
        return <>{children}</>;
    }
    return (
        <LinkLookalike always onClick={() => setOpen(true)}>
            {r.translate("Show more information")}
        </LinkLookalike>
    );
}

function LinkLookalike({ children, onClick, always }) {
    const classes = ["linkLike"];
    if (always === true) {
        classes.push("linkLike-always");
    }
    return (
        <span className={classes.join(" ")} onClick={onClick}>
            {children}
        </span>
    );
}

export { CollectionItem, CollectionItemTableRow };
