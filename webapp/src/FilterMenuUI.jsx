/*
 * rotcelloc filter UI
 *
 * Part of rotcelloc - the hacker's movie, tv-series and game collection
 * manager
 *
 * Copyright (C) Eskild Hustvedt 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import "./App.scss";
import { useState } from "react";
import React from "react";
import { getCollectionInstance, getRotcelloc } from "./rotcellocData.js";
import {
    genreSearchType,
    displayAsTable,
    searchInPlot,
    searchString,
    showOnlyFormats,
    showOnlyGenres,
    showOnlyGroup,
    showOnlyPlatforms,
    showOnlyWatched,
    showGroupAboveTheFold,
    sortOrder,
    toggleGroupInOnlyFormats,
    toggleGroupInOnlyGenres,
    toggleGroupInOnlyPlatforms,
    visibleItems,
} from "./State.js";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
    Button,
    Card,
    Col,
    Collapse,
    FormGroup,
    Input,
    InputGroup,
    InputGroupText,
    Label,
    Row,
} from "reactstrap";

function FilterMenu() {
    const r = getRotcelloc();
    const [isOpen, setIsOpen] = useState(false);
    return (
        <>
            <Row className="my-2">
                <Col>
                    <Button
                        onClick={() => {
                            setIsOpen(!isOpen);
                        }}
                    >
                        {r.translate("Show filter")}
                    </Button>
                </Col>
                <Col
                    md={{
                        size: 3,
                        offset: 4,
                    }}
                >
                    <SortOrderSelector />
                </Col>
                <Col md="3">
                    <SearchBox />
                </Col>
            </Row>
            <Row>
                <Collapse isOpen={isOpen}>
                    <div className="mb-2">
                        <AdditionalFilters />
                    </div>
                </Collapse>
            </Row>
        </>
    );
}

function AdditionalFilters() {
    return (
        <Card color="light">
            <div className="p-2">
                <GroupFilters />
                <FormatFilters />
                <PlatformFilters />
                <GenreFilters />
                <AdditionalSearchOptionsFilters />
            </div>
        </Card>
    );
}

function FilterEntry({ children }) {
    return <div className="p-1">{children}</div>;
}

function GroupFilters() {
    const c = getCollectionInstance();
    const r = getRotcelloc();
    if (c.workingConfig.sources.length <= 1) {
        return null;
    }
    const [currentShowOnlyGroup, setShowOnlyGroup] =
        useRecoilState(showOnlyGroup);
    const sources = [
        {
            value: "",
            name: r.translate("All"),
        },
    ];
    for (const source of c.workingConfig.sources) {
        sources.push({
            value: source.bSource,
            name: source.name,
        });
    }
    return (
        <FilterEntry>
            <Row>
                <Col className="fw-bold">{r.translate("Group")}</Col>
            </Row>
            <Row>
                <Col>
                    <ButtonBar
                        buttons={sources}
                        selected={currentShowOnlyGroup}
                        size="sm"
                        pushSelected={(value) => setShowOnlyGroup(value)}
                    />
                </Col>
            </Row>
        </FilterEntry>
    );
}

function FormatFilters() {
    const c = getCollectionInstance();
    const r = getRotcelloc();
    const formatButtons = [];
    const [currentFormats] = useRecoilState(showOnlyFormats);
    const toggleCurrentFormats = useSetRecoilState(toggleGroupInOnlyFormats);
    if (c.workingMeta.formats && c.workingMeta.formats.length) {
        for (
            let formatN = 0;
            formatN < c.workingMeta.formats.length;
            formatN++
        ) {
            const format = c.workingMeta.formats[formatN];
            formatButtons.push({
                value: format,
                name: format,
            });
        }
    }
    if (formatButtons.length <= 1) {
        return null;
    }
    return (
        <FilterEntry>
            <Row>
                <Col className="fw-bold">{r.translate("Format")}</Col>
            </Row>
            <Row>
                <Col>
                    <ButtonBar
                        buttons={formatButtons}
                        selected={currentFormats}
                        size="sm"
                        pushSelected={(value) => toggleCurrentFormats(value)}
                    />
                </Col>
            </Row>
        </FilterEntry>
    );
}

function GenreFilters() {
    const genreButtons = [];
    const c = getCollectionInstance();
    const r = getRotcelloc();
    if (c.workingMeta.genres && c.workingMeta.genres.length) {
        for (let genreN = 0; genreN < c.workingMeta.genres.length; genreN++) {
            const genre = c.workingMeta.genres[genreN];
            genreButtons.push({
                id: "genre_" + genre,
                value: genre,
                name: genre,
            });
        }
    }
    if (!genreButtons.length) {
        return null;
    }
    const genreType = [
        {
            value: "all",
            name: r.translate("In genre (all selected)"),
        },
        {
            value: "any",
            name: r.translate("In genre (any selected)"),
        },
        {
            value: "notin",
            name: r.translate("Not in genre"),
        },
    ];
    const [currentShowOnlyGenres] = useRecoilState(showOnlyGenres);
    const [currentGenreSearchType, setGenreSearchType] =
        useRecoilState(genreSearchType);
    const toggleCurrentGenres = useSetRecoilState(toggleGroupInOnlyGenres);
    return (
        <FilterEntry>
            <Row>
                <Col md="3">
                    <SelectBoxFromDataset
                        data={genreType}
                        selected={currentGenreSearchType}
                        onChange={() => setGenreSearchType(event.target.value)}
                        requireEnabled={false}
                        bold
                    />
                </Col>
            </Row>
            <Row>
                <Col>
                    <ButtonBar
                        buttons={genreButtons}
                        selected={currentShowOnlyGenres}
                        size="sm"
                        pushSelected={(value) => toggleCurrentGenres(value)}
                    />
                </Col>
            </Row>
        </FilterEntry>
    );
}

function PlatformFilters() {
    const platformButtons = [];
    const c = getCollectionInstance();
    const r = getRotcelloc();
    if (c.workingMeta.platforms && c.workingMeta.platforms.length) {
        for (
            let platformN = 0;
            platformN < c.workingMeta.platforms.length;
            platformN++
        ) {
            const platform = c.workingMeta.platforms[platformN];
            platformButtons.push({
                id: "platform_" + platform,
                value: platform,
                name: platform,
            });
        }
    }
    if (!platformButtons.length) {
        return null;
    }
    const [currentShowOnlyPlatforms] = useRecoilState(showOnlyPlatforms);
    const toggleCurrentPlatforms = useSetRecoilState(
        toggleGroupInOnlyPlatforms,
    );
    return (
        <FilterEntry>
            <Row className="fw-bold">
                <Col>{r.translate("Platform")}</Col>
            </Row>
            <Row>
                <Col>
                    <ButtonBar
                        buttons={platformButtons}
                        selected={currentShowOnlyPlatforms}
                        size="sm"
                        pushSelected={(value) => toggleCurrentPlatforms(value)}
                    />
                </Col>
            </Row>
        </FilterEntry>
    );
}

function AdditionalSearchOptionsFilters() {
    const r = getRotcelloc();
    const [currentShowGroupAboveTheFold, setShowGroupAboveTheFold] =
        useRecoilState(showGroupAboveTheFold);
    const [currentOnlyWatched, setOnlyWatched] =
        useRecoilState(showOnlyWatched);
    const [currentDisplayAsTable, setDisplayAsTable] =
        useRecoilState(displayAsTable);
    const [currentSearchInPlot, setSearchInPlot] = useRecoilState(searchInPlot);
    return (
        <FilterEntry>
            <Row className="no-select">
                <Col>
                    <FormGroup switch className="d-inline-block">
                        <Input
                            type="switch"
                            role="switch"
                            checked={currentSearchInPlot}
                            onClick={() =>
                                setSearchInPlot(!currentSearchInPlot)
                            }
                        />
                        <Label
                            check
                            onClick={() =>
                                setSearchInPlot(!currentSearchInPlot)
                            }
                        >
                            {r.translate("Search in plot descriptions")}
                        </Label>
                    </FormGroup>
                    <FormGroup switch className="d-inline-block ms-2">
                        <Input
                            type="switch"
                            role="switch"
                            checked={currentOnlyWatched}
                            onClick={() => setOnlyWatched(!currentOnlyWatched)}
                        />
                        <Label
                            check
                            onClick={() => setOnlyWatched(!currentOnlyWatched)}
                        >
                            {r.translate("Only display unwatched titles")}
                        </Label>
                    </FormGroup>
                    <FormGroup switch className="d-inline-block ms-2">
                        <Input
                            type="switch"
                            role="switch"
                            checked={currentShowGroupAboveTheFold}
                            onClick={() =>
                                setShowGroupAboveTheFold(
                                    !currentShowGroupAboveTheFold,
                                )
                            }
                        />
                        <Label
                            check
                            onClick={() =>
                                setShowGroupAboveTheFold(
                                    !currentShowGroupAboveTheFold,
                                )
                            }
                        >
                            {r.translate("Always show groups")}
                        </Label>
                    </FormGroup>
                    <FormGroup switch className="d-inline-block ms-2">
                        <Input
                            type="switch"
                            role="switch"
                            checked={currentDisplayAsTable}
                            onClick={() =>
                                setDisplayAsTable(!currentDisplayAsTable)
                            }
                        />
                        <Label
                            check
                            onClick={() =>
                                setDisplayAsTable(!currentDisplayAsTable)
                            }
                        >
                            {r.translate("Show in a table")}
                        </Label>
                    </FormGroup>
                </Col>
            </Row>
        </FilterEntry>
    );
}

function ButtonBar({ buttons, selected, pushSelected, size }) {
    const r = getRotcelloc();
    const selectedItems = {};
    if (Array.isArray(selected)) {
        selected.forEach((element) => (selectedItems[element] = true));
    } else {
        selectedItems[selected] = true;
    }
    const setVisibleItems = useSetRecoilState(visibleItems);
    const renderButtons = [];
    for (let n = 0; n < buttons.length; n++) {
        const button = buttons[n];
        renderButtons.push(
            <Button
                key={"button-" + n}
                active={selectedItems[button.value] === true}
                onClick={() => {
                    pushSelected(button.value);
                    setVisibleItems(r.maxEntriesPerRenderedPage);
                }}
                size={size}
                outline
            >
                {button.name}
            </Button>,
        );
    }
    return <div className="buttonbar">{renderButtons}</div>;
}

function SearchBox() {
    const [currentSearchString, setSearchString] = useRecoilState(searchString);
    const setVisibleItems = useSetRecoilState(visibleItems);
    const setSortOrder = useSetRecoilState(sortOrder);
    const r = getRotcelloc();
    return (
        <InputGroup>
            <Input
                name="search"
                placeholder={r.translate("Search")}
                type="search"
                value={currentSearchString}
                autoFocus={!r.mobile}
                onChange={(event) => {
                    setSearchString(event.target.value);
                    if (event.target.value !== "") {
                        setSortOrder("score");
                    } else {
                        const c = getCollectionInstance();
                        setSortOrder(c.workingConfig.defaultSort);
                    }
                    setVisibleItems(r.maxEntriesPerRenderedPage);
                }}
            />
            <Button
                disabled={currentSearchString === ""}
                onClick={() => {
                    setSearchString("");
                    const c = getCollectionInstance();
                    setSortOrder(c.workingConfig.defaultSort);
                    setVisibleItems(r.maxEntriesPerRenderedPage);
                }}
            >
                X
            </Button>
        </InputGroup>
    );
}

function SortOrderSelector() {
    const c = getCollectionInstance();
    const r = getRotcelloc();
    const [currentSearchString] = useRecoilState(searchString);
    const orderButtons = [
        {
            id: "order_none",
            value: "",
            name: r.translate("Automatic"),
            renderWhen: true, // always
        },
        {
            id: "sortableAuthor",
            value: "sortableAuthor",
            name: r.translate("Author"),
            renderWhen: c.workingMeta.type === "books",
        },
        {
            id: "order_alpha",
            value: "alpha",
            name: r.translate("Alphabetic"),
            renderWhen: c.workingMeta.hasDisneySort,
        },
        {
            id: "order_year",
            value: "year",
            name: r.translate("Year"),
            renderWhen: c.workingMeta.fields.year,
        },
        {
            id: "order_score",
            value: "score",
            name: r.translate("Score"),
            renderWhen: currentSearchString !== "",
        },
        {
            id: "order_added",
            value: "added",
            name: r.translate("Date added"),
            renderWhen: c.workingMeta.fields.added,
        },
        {
            id: "order_rand",
            value: "random",
            name: r.translate("Random"),
            renderWhen:
                c.workingMeta.type === "movies" ||
                c.workingMeta.type === "series",
        },
        {
            id: "runtime",
            value: "runtimeMin",
            name: r.translate("Length"),
            renderWhen:
                c.workingMeta.type === "movies" ||
                c.workingMeta.type === "series",
        },
        {
            id: "order_normalRating",
            value: "normalizedRating",
            name: r.translate("Rating (smart)"),
            renderWhen: c.workingMeta.enableNormalized,
        },
        {
            id: "order_rating",
            value: "rating",
            name: r.translate("Custom rating"),
            renderWhen: c.workingMeta.fields.rating,
        },
        {
            id: "order_meta",
            value: "metascore",
            name: r.translate("Metascore"),
            renderWhen: c.workingMeta.fields.metascore,
        },
        {
            id: "order_imdb",
            value: "imdbRating",
            name: r.translate("IMDB rating"),
            renderWhen:
                c.workingMeta.type === "movies" ||
                c.workingMeta.type === "series",
        },
    ];
    // FIXME: We should handle this in some declarative manner inside
    // the definition for order_none
    if (!c.workingMeta.hasDisneySort) {
        orderButtons[0].name = r.translate("Alphabetic");
        orderButtons[0].value = "alpha";
    }
    const [currentSortOrder, setSortOrder] = useRecoilState(sortOrder);
    return (
        <SelectBoxFromDataset
            data={orderButtons}
            selected={currentSortOrder}
            onChange={(event) => {
                setSortOrder(event.target.value);
            }}
            label={r.translate("Order")}
        />
    );
}

function SelectBoxFromDataset({
    data,
    selected,
    requireEnabled,
    onChange,
    label,
    bold,
}) {
    const options = [];
    for (const button of data) {
        if (button.renderWhen || requireEnabled === false) {
            options.push(
                <option value={button.value} key={"option-" + button.value}>
                    {button.name}
                </option>,
            );
        }
    }
    let TextLabel = null;
    if (label !== undefined) {
        TextLabel = <InputGroupText>{label}</InputGroupText>;
    }
    return (
        <InputGroup size="10em">
            {TextLabel}
            <select
                className={"form-select" + (bold ? " fw-bold" : "")}
                value={selected}
                onChange={onChange}
            >
                {options}
            </select>
        </InputGroup>
    );
}

export { FilterMenu };
