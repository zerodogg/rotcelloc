/*
 * Rotcelloc state management
 *
 * Part of rotcelloc - the hacker's movie, tv-series and game collection
 * manager
 *
 * Copyright (C) Eskild Hustvedt 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { atom, selector } from "recoil";
import { getCollectionInstance } from "./rotcellocData.js";

const visibleItems = atom({
    key: "visibleItems",
    default: 15,
});
const showGroupAboveTheFold = atom({
    key: "showGroupAboveTheFold",
    default: false,
});
const searchString = atom({
    key: "searchString",
    default: "",
});
const showOnlyWatched = atom({
    key: "showOnlyWatched",
    default: false,
});
const searchInPlot = atom({
    key: "searchInPlot",
    default: false,
});
const displayAsTable = atom({
    key: "displayAsTable",
    default: false,
});
const sortOrder = atom({
    key: "sortOrder",
    default: "year",
});
const showOnlyGroup = atom({
    key: "showOnlyGroup",
    default: "",
});
const showOnlyFormats = atom({
    key: "showOnlyFormats",
    default: [],
});
const toggleGroupInOnlyFormats = selector({
    key: "toggleGroupInOnlyFormats",
    get: ({ _get }) => {
        throw new Error("not implemented");
    },
    set: ({ set, get }, group) => {
        const current = get(showOnlyFormats);
        if (current.indexOf(group) !== -1) {
            set(
                showOnlyFormats,
                current.filter((cur) => cur !== group),
            );
        } else {
            set(showOnlyFormats, current.concat(group));
        }
    },
});
const genreSearchType = atom({
    key: "genreSearchType",
    default: "all",
});
const showOnlyGenres = atom({
    key: "showOnlyGenres",
    default: [],
});
const toggleGroupInOnlyGenres = selector({
    key: "toggleGroupInOnlyGenres",
    get: ({ _get }) => {
        throw new Error("not implemented");
    },
    set: ({ set, get }, group) => {
        const current = get(showOnlyGenres);
        if (current.indexOf(group) !== -1) {
            set(
                showOnlyGenres,
                current.filter((cur) => cur !== group),
            );
        } else {
            set(showOnlyGenres, current.concat(group));
        }
    },
});
const showOnlyPlatforms = atom({
    key: "showOnlyPlatforms",
    default: [],
});
const toggleGroupInOnlyPlatforms = selector({
    key: "toggleGroupInOnlyPlatforms",
    get: ({ _get }) => {
        throw new Error("not implemented");
    },
    set: ({ set, get }, group) => {
        const current = get(showOnlyPlatforms);
        if (current.indexOf(group) !== -1) {
            set(
                showOnlyPlatforms,
                current.filter((cur) => cur !== group),
            );
        } else {
            set(showOnlyPlatforms, current.concat(group));
        }
    },
});
const bumpVisibleItems = selector({
    key: "bumpVisibleItems",
    get: ({ _get }) => {
        throw new Error("not implemented");
    },
    set: ({ set, get }, bumpBy) => {
        const current = get(visibleItems);
        const c = getCollectionInstance();
        if (c.prevQueryEntries > current) {
            set(visibleItems, get(visibleItems) + bumpBy);
        }
    },
});
export {
    bumpVisibleItems,
    displayAsTable,
    genreSearchType,
    searchInPlot,
    searchString,
    showOnlyFormats,
    showOnlyGenres,
    showOnlyGroup,
    showOnlyWatched,
    showGroupAboveTheFold,
    sortOrder,
    toggleGroupInOnlyFormats,
    toggleGroupInOnlyGenres,
    visibleItems,
    showOnlyPlatforms,
    toggleGroupInOnlyPlatforms,
};
