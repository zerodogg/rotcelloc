/*
 * @flow
 *
 * Data reader classes
 *
 * Part of rotcelloc - the hacker's movie, tv-series and game collection
 * manager
 *
 * Copyright (C) Eskild Hustvedt 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import rotcellocSearcher from "./rotcellocSearcher.js";
class rotcellocCollection {
    /*
     * Initializes the page:
     * - Sets up object variables
     * - Loads data
     * - Performs initial render
     * - Sets up event handlers
     */
    constructor(collectionName) {
        this.ready = false;
        this.collectionName = collectionName;
        this.prevQueryEntries = 0;
        this._onInitialized = [];
        this.getDataSet().then((data) => {
            this.i18n = data.i18n;

            this.searcher = new rotcellocSearcher(
                this.workingData,
                this.workingConfig,
            );
            this.ready = true;
            for (const initFN of this._onInitialized) {
                // FIXME: Handle exceptions
                initFN();
            }
        });
    }
    /*
     * Downloads and prepares the dataset for the collection on the current page
     */
    async getDataSet() {
        const response = await fetch(
            getRotcelloc().getPathToCollectionFile(this.collectionName),
        );
        const data = await response.json();
        this.data = data;
        this.pagetype = getRotcelloc().getCollectionNameFromPath(
            this.collectionName,
        );
        this.workingData = data.data;
        this.workingMeta = data.meta;
        this.workingConfig = this.data.config.collections[this.pagetype];
        this.config = this.data.config;
        if (this.data.dVer !== 1) {
            throw (
                "ERROR: Dataset is of an unsupported version: " + this.data.dVer
            );
        }
        return data;
    }

    /*
     * Runs a function once an object is initialized. If it has already been
     * initialized the function is run immediately.
     */
    onInitialized(fn) {
        if (this.ready) {
            fn();
            return;
        }
        this._onInitialized.push(fn);
    }
    /*
     * Searches our dataset, handling many different fields and scoring hits
     * appropriately
     */
    performSearch(query) {
        const result = this.searcher.search(query);
        this.prevQueryEntries = result.length;
        return result;
    }

    /*
     * Sets the page title. Useful because it also tracks the original title
     * for us and just appends whatever we supply to this function to the
     * original title
     */
    setPageTitle(_add) {
        throw "setPageTitle: STUB";
    }

    translate(str) {
        console.log(
            "Warning: use of rotcellocCollection.translate(), just returning the string",
        );
        return str;
    }
}

class rotcellocWeb {
    constructor() {
        this.ready = false;
        this._collectionObjects = {};
        this._onInitialized = [];
        this._selectedInstance = null;
        this.getDataSet().then(() => {
            this.ready = true;
            for (const initFN of this._onInitialized) {
                // FIXME: Handle exceptions
                initFN();
            }
        });
    }

    /*
     * Downloads and prepares the index
     */
    async getDataSet() {
        const response = await fetch("/rotcelloc-index.json");
        const data = await response.json();
        this._index = data;
        if (/(Android|Mobile|iOS|iPhone)/.test(navigator.userAgent)) {
            this.maxEntriesPerRenderedPage =
                data.config.maxEntriesPerRenderedPageMobile;
            this.mobile = true;
        } else {
            this.maxEntriesPerRenderedPage =
                data.config.maxEntriesPerRenderedPage;
            this.mobile = false;
        }
        return data;
    }

    parseCollectionFromPath(path) {
        const paths = path.split("/");
        let cleaned = null;
        if (paths.length === 1) {
            cleaned = paths[0];
        } else if (
            paths[0] === "" &&
            (paths.length === 2 ||
                (paths.length === 3 && paths[0] === "" && paths[2] === ""))
        ) {
            cleaned = paths[1];
        }
        if (cleaned.indexOf(".html") !== -1) {
            cleaned = cleaned.replace(/\.html$/, "");
        }
        if (cleaned !== null) {
            return decodeURIComponent(cleaned).toLowerCase();
        }
        return null;
    }

    isValidPath(path) {
        const cleaned = this.parseCollectionFromPath(path);
        if (cleaned === null) {
            return false;
        }
        return this.getCollectionNameFromPath(cleaned) !== null;
    }

    getCollectionNameFromPath(path) {
        const checkPath = this.parseCollectionFromPath(path);
        if (checkPath === null) {
            return null;
        }
        if (this.index().pathsToCategories[checkPath] !== undefined) {
            return this.index().pathsToCategories[checkPath];
        }
        return null;
    }

    hasInitializedCollection(collectionName) {
        return this._collectionObjects[collectionName] !== undefined;
    }

    initializeCollection(collectionName, doneCallback) {
        let instance;
        if (this.hasInitializedCollection(collectionName)) {
            instance = this.getCollectionInstanceFor(collectionName);
        } else {
            instance = this._collectionObjects[collectionName] =
                new rotcellocCollection(collectionName);
            this._collectionObjects[collectionName] = instance;
        }
        this._selectedInstance = instance;
        instance.onInitialized(() => {
            doneCallback(instance);
        });
    }

    initializeDefaultCollection(doneCallback) {
        return this.initializeCollection(
            this.getDefaultCollection(),
            doneCallback,
        );
    }

    getDefaultCollection() {
        if (
            this.index().defaultCollection === null ||
            this.index().defaultCollection === undefined
        ) {
            throw "No default collection in config";
        }
        return this.index().categoriesToPaths[this.index().defaultCollection];
    }

    getPathFromCategory(category) {
        return this.index().categoriesToPaths[category];
    }

    getCollectionInstanceFor(collectionName) {
        return this._collectionObjects[collectionName];
    }

    getCollectionInstance() {
        if (
            this._selectedInstance === undefined ||
            this._selectedInstance === null
        ) {
            throw "Attempt to .getCollectionInstance() with no active instance";
        }
        return this._selectedInstance;
    }

    getPathToCollectionFile(collectionName) {
        if (this.isValidPath(collectionName)) {
            return (
                "/" +
                this.index().categoriesToFiles[collectionName] +
                "?cs=" +
                this.index().checksums[collectionName]
            );
        }
        return null;
    }

    getCurrenctCollectionIdentifier() {
        if (
            this._selectedInstance === undefined ||
            this._selectedInstance === null
        ) {
            return null;
        }
        return this._selectedInstance.pagetype;
    }

    /*
     * Runs a function once an object is initialized. If it has already been
     * initialized the function is run immediately.
     */
    onInitialized(fn) {
        if (this.ready) {
            fn();
            return;
        }
        this._onInitialized.push(fn);
    }

    index() {
        if (this._index === undefined) {
            throw "Attempt to use index before it has loaded";
        }
        return this._index;
    }

    translate(str) {
        if (this.index().i18n[str] !== undefined) {
            return this.index().i18n[str];
        }
        return str;
    }
}

function getRotcelloc() {
    if (window.__rotcelloc === undefined) {
        window.__rotcelloc = new rotcellocWeb();
    }
    return window.__rotcelloc;
}

function getCollectionInstance() {
    return getRotcelloc().getCollectionInstance();
}
export {
    getCollectionInstance,
    getRotcelloc,
    rotcellocCollection,
    rotcellocWeb,
};
