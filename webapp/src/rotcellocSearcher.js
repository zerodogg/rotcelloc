/*
 * Rotcelloc search engine
 *
 * Part of rotcelloc - the hacker's movie, tv-series and game collection
 * manager
 *
 * Copyright (C) Eskild Hustvedt 2015-2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import shuffle from "lodash/shuffle";
/*
 * Our actual search engine
 */
function warn(msg) {
    console.log(msg);
}
export default class rotcellocSearcher {
    constructor(workingData, workingConfig) {
        this.workingData = workingData;
        this.workingConfig = workingConfig;
        this.lastResults = null;
        this.lastResultsCkey = null;
        this.lastResultsText = null;
    }

    /*
     * Performs a search. Expects an object as returned from
     * rotcellocFiltersListRenderer.getSearch().
     *
     * This returns an array of results
     */
    search(query) {
        let results = [];
        const types = [
            "group",
            "watchedSearch",
            "formats",
            "genres",
            "platform",
            "text",
        ];

        let ckey = "-";

        // This is a hack that invalidates the cache if the search query looks
        // like a four-digit number (assumed to be a year)
        if (
            query.text != null &&
            query.text.length === 4 &&
            !isNaN(parseInt(query.text))
        ) {
            ckey += "-yearCacheBurst-";
        }

        for (let typeN = 0; typeN < types.length; typeN++) {
            const type = types[typeN];
            if (type !== "text" && query[type] !== undefined) {
                ckey += "||" + type + "==" + query[type] + "||";
            }
        }

        let searchData;

        if (
            this.lastResultsCkey === ckey &&
            query.text != null &&
            this.lastResultsText != null &&
            query.text.startsWith(this.lastResultsText)
        ) {
            searchData = this.lastResults;
        } else {
            searchData = this.workingData;
        }

        const cache = {};
        for (
            let collectionN = 0;
            collectionN < searchData.length;
            collectionN++
        ) {
            const collectionEntry = searchData[collectionN];
            const resultMeta = { score: 0 };
            let hit = true;
            for (let typeN = 0; typeN < types.length; typeN++) {
                const type = types[typeN];
                if (
                    this.trySearch(
                        collectionEntry,
                        type,
                        query,
                        resultMeta,
                        cache,
                    ) === false
                ) {
                    hit = false;
                    break;
                }
            }
            if (!hit) {
                continue;
            }
            // FIXME
            collectionEntry.searchScore = resultMeta.score;
            results.push(collectionEntry);
        }
        results = this.sortResults(results, query.order, query);
        this.lastResults = results;
        this.lastResultsCkey = ckey;
        this.lastResultsText = query.text;
        return results;
    }

    /*
     * This is a wrapper function that handles calling all of the
     * individual query* methods and handles checking if we need to perform
     * said query, bumps the score if needed etc.
     */
    trySearch(collectionEntry, type, rawQuery, resultMeta, cache) {
        const query = rawQuery[type];
        if (query === undefined) {
            return true;
        }
        let result = { hit: false };
        if (type === "group") {
            result = this.queryGroup(collectionEntry, query, rawQuery);
        } else if (type === "watchedSearch") {
            result = this.queryWatched(collectionEntry, query, rawQuery);
        } else if (type === "formats") {
            result = this.queryFormats(collectionEntry, query, rawQuery);
        } else if (type === "platform") {
            result = this.queryPlatform(collectionEntry, query, rawQuery);
        } else if (type === "genres") {
            result = this.queryGenres(collectionEntry, query, rawQuery);
        } else if (type === "text") {
            result = this.queryText(collectionEntry, query, rawQuery, cache);
        } else {
            warn("Unhandled search type: " + type);
        }
        if (result.hit === true) {
            if (result.scoreMod !== undefined) {
                resultMeta.score += result.scoreMod;
            }
            return true;
        }
        return false;
    }

    /*
     * Performs sorting of the search results
     */
    sortResults(results, order, rawQuery) {
        let sorted = false;
        if (order) {
            /**
             * This function takes a list of optFields and builds a function
             * that can sort by those fields. optFields is an array of objects
             * where each object has a "field" which is the field to sort by
             * and "orderBy" which is either "numeric" or "text".
             *
             * If optFields is NOT an array, it is converted to a single
             * element array with the optFields value as its only member.
             */
            const orderByOptionalField = function (optFields) {
                if (!Array.isArray(optFields)) {
                    optFields = [optFields];
                }
                console.log(
                    "orderByOptionalField: " + JSON.stringify(optFields),
                );
                return function (a, b) {
                    for (const entry of optFields) {
                        const optField = entry.field;
                        const orderType = entry.orderBy;
                        const reverse = entry.reverse;
                        if (
                            a[optField] &&
                            b[optField] &&
                            a[optField] !== b[optField]
                        ) {
                            let first = a;
                            let second = b;
                            if (reverse === true) {
                                first = b;
                                second = a;
                            }
                            if (first[optField] === second[optField]) {
                                continue;
                            }
                            if (orderType === "text") {
                                return first[optField].localeCompare(
                                    second[optField],
                                );
                            }
                            return second[optField] - first[optField];
                        }
                        if (a[optField] && !b[optField]) {
                            return -1;
                        } else if (!a[optField] && b[optField]) {
                            return 1;
                        }
                    }
                    return a.title.localeCompare(b.title);
                };
            };
            if (order === "alpha") {
                sorted = true;
                results.sort((a, b) => {
                    return a.title.localeCompare(b.title, "no-nn");
                });
            } else if (order === "random") {
                sorted = true;
                results = shuffle(results);
            } else if (
                this.workingConfig.type === "books" &&
                order === "year"
            ) {
                sorted = true;
                results.sort(
                    orderByOptionalField([
                        { field: order, orderBy: "numeric" },
                        { field: "note", orderBy: "text", reverse: true },
                        { field: "isbn", orderBy: "text" },
                    ]),
                );
            } else if (
                order === "rating" ||
                order === "imdbRating" ||
                order === "metascore" ||
                order === "runtimeMin" ||
                order === "year" ||
                order === "added" ||
                order === "normalizedRating"
            ) {
                sorted = true;
                results.sort(
                    orderByOptionalField({ field: order, orderBy: "numeric" }),
                );
            } else if (order === "sortableAuthor") {
                sorted = true;
                results.sort(
                    orderByOptionalField({ field: order, orderBy: "text" }),
                );
            } else if (order !== "score") {
                warn("Skipping sorting by unknown method: " + order);
            }
        }
        if (sorted === false) {
            results.sort((a, b) => {
                if (a.searchScore !== b.searchScore) {
                    return b.searchScore - a.searchScore;
                }
                if (rawQuery.disneySort) {
                    if (a.disneyClassicNo && b.disneyClassicNo) {
                        return a.disneyClassicNo - b.disneyClassicNo;
                    }
                    if (a.disneyClassicNo && !b.disneyClassicNo) {
                        return -1;
                    }
                    if (!a.disneyClassicNo && b.disneyClassicNo) {
                        return 1;
                    }
                }
                if (
                    a.year !== null &&
                    a.year !== undefined &&
                    b.year !== null &&
                    b.year !== undefined &&
                    a.year !== b.year
                ) {
                    return b.year - a.year;
                }
                if (!a.title) {
                    throw { message: "Got title-less entry", entry: a };
                }
                return a.title.localeCompare(b.title);
            });
        }
        return results;
    }

    /*
     * Custom queries. These are not available in the interface itself, but
     * can be used to debug issues with a collection from the developer
     * console in the browser
     */
    queryCustom(collectionEntry, custom) {
        const ret = { hit: false };
        if (custom.type === "substr") {
            if (collectionEntry[custom.field].includes(custom.text)) {
                ret.hit = true;
            }
        } else if (custom.type === "not-defined") {
            if (collectionEntry[custom.field] === undefined) {
                ret.hit = true;
            }
        } else if (custom.type === "defined") {
            if (collectionEntry[custom.field] !== undefined) {
                ret.hit = true;
            }
        }
        return ret;
    }

    scoreTextMatch(
        inString,
        lowerInString,
        searchTextLower,
        searchText,
        baseMod,
        cachePrefix,
        collectionEntry,
        cache,
        extraModifier,
    ) {
        if (extraModifier === undefined || extraModifier === null) {
            extraModifier = 0;
        }
        let scoreMod = baseMod;
        // First, see if casing matches, if it does, grant bonus points
        if (inString.includes(searchText)) {
            scoreMod += 20;
        }
        /*
         * Case insensitive check for if the title starts with our search string,
         * if it does grant some bonus points
         */
        if (lowerInString.startsWith(searchTextLower)) {
            scoreMod += 75 + extraModifier;
        }
        /*
         * Next, split both strings by non-words, and then check to see if
         * there are full string matches, or "start of string" matches
         * in each of the arrays, and grant points accordingly.
         * This is done case insensitively.
         */
        const splitInStringCache = cachePrefix + "_splitInString";
        collectionEntry[splitInStringCache] ||= this.cleanSplit(lowerInString);
        cache.splitText ||= this.cleanSplit(searchTextLower);
        const splitInString = collectionEntry[splitInStringCache];
        const splitSearchText = cache.splitText;
        for (const textEntry of splitSearchText) {
            for (const stringEntry of splitInString) {
                if (stringEntry === textEntry) {
                    scoreMod += 100;
                } else if (stringEntry.startsWith(textEntry)) {
                    scoreMod += 30;
                }
            }
        }
        // If the strings match exactly, grant more bonus points
        if (lowerInString === searchTextLower) {
            scoreMod += 100 + extraModifier;
        }
        // Finally, apply a tiny penalty depending on the length of
        // the inString
        scoreMod -= inString.length;

        return scoreMod;
    }

    /*
     * Performs free-text searches
     */
    queryText(collectionEntry, text, rawQuery, cache) {
        const ret = { hit: false, scoreMod: 0 };
        const textTypes = [
            {
                source: "title",
                score: 2030,
                extraModifier: 700,
            },
            {
                source: "origTitle",
                score: 2030,
                extraModifier: 700,
            },
            {
                source: "altTitle",
                score: 2000,
                extraModifier: 700,
            },
            {
                source: "actors",
                score: 30,
            },
            {
                source: "writer",
                score: 30,
            },
            {
                source: "author",
                score: 30,
            },
            {
                source: "director",
                score: 30,
            },
            {
                source: "developer",
                score: 30,
            },
            {
                source: "genre",
                score: 20,
            },
            {
                source: "year",
                type: "numeric",
                scoreMode: "basic",
                score: 10,
            },
            {
                source: "bSource",
                score: 10,
                scoreMode: "basic",
            },
            {
                source: "plot",
                onlyIf: rawQuery.plotSearch === true,
                score: 5,
                scoreMode: "basic",
            },
            {
                source: "note",
                score: 1,
                scoreMode: "basic",
            },
            {
                source: "language",
                score: 1,
                scoreMode: "basic",
            },
            {
                source: "publisher",
                score: 1,
                scoreMode: "basic",
            },
        ];
        if (!cache.splitText) {
            cache.splitText = this.cleanSplit(text);
        }
        for (const entry of textTypes) {
            if (entry.onlyIf === false) {
                continue;
            }
            const cacheNameLC = "_lcCache_" + entry.source;
            let match = false;
            if (collectionEntry[entry.source] == null) {
                continue;
            }
            if (entry.type === "numeric") {
                // Doing == not === because we want conversions
                // eslint-disable-next-line
                if (collectionEntry[entry.source] == text) {
                    match = true;
                }
            } else {
                if (collectionEntry[cacheNameLC] === undefined) {
                    collectionEntry[cacheNameLC] =
                        collectionEntry[entry.source].toLowerCase();
                }
                if (collectionEntry[cacheNameLC].includes(text)) {
                    match = true;
                } else if (
                    cache.splitText.length > 1 &&
                    cache.splitText.every((word) =>
                        collectionEntry[cacheNameLC].includes(word),
                    )
                ) {
                    match = true;
                }
            }

            if (match) {
                ret.hit = true;
                ret.scoreMod =
                    entry.scoreMode === "basic"
                        ? entry.score
                        : this.scoreTextMatch(
                              collectionEntry[entry.source],
                              collectionEntry[cacheNameLC],
                              text,
                              rawQuery.rawText,
                              entry.score,
                              "_c_STM_" + entry.source,
                              collectionEntry,
                              cache,
                          );
                // We use only the first ("best") result, so don't process any
                // others. If we processed more then a movie with more than one
                // title will have an advantage, since it has more chances for
                // hits. It would also be a disadvantage for entries where for
                // instance both title and actors (which is a long string)
                // matches, and where the length of the actors string could
                // null out hits in the title (since scores are penalized by
                // string length in scoreTextMatch).
                break;
            }
        }
        return ret;
    }

    /*
     * Splits str into an array of words
     */
    cleanSplit(str) {
        return str.replace(/^\s+/, "").replace(/\s+$/, "").split(/\s+/);
    }

    /*
     * Queries for matches in the format list
     */
    queryFormats(collectionEntry, formats) {
        const ret = { hit: true };
        if (!collectionEntry.format) {
            ret.hit = false;
        } else {
            ret.hit = this.inArrays(formats, collectionEntry.format);
        }
        return ret;
    }

    /*
     * Queries for matches in genres
     */
    queryGenres(collectionEntry, query, rawQuery) {
        const ret = { hit: true };
        if (
            rawQuery.genreSearchType === "all" ||
            rawQuery.genreSearchType === "any"
        ) {
            if (!collectionEntry.genre) {
                ret.hit = false;
                return ret;
            }
            if (rawQuery.genreSearchType === "any") {
                ret.hit = this.inArrays(
                    rawQuery.genres,
                    collectionEntry.genres,
                );
            } else {
                for (const genreI in rawQuery.genres) {
                    if (
                        !this.inArray(
                            collectionEntry.genres,
                            rawQuery.genres[genreI],
                        )
                    ) {
                        ret.hit = false;
                        continue;
                    }
                }
            }
        } else if (rawQuery.genreSearchType === "notin") {
            ret.hit = true;
            if (collectionEntry.genre) {
                ret.hit = !this.inArrays(
                    rawQuery.genres,
                    collectionEntry.genres,
                );
            }
        }
        return ret;
    }

    /*
     * Queries for matches in platfrom
     */
    queryPlatform(collectionEntry, query, rawQuery) {
        return {
            hit: this.arrayInString(
                rawQuery.platform,
                collectionEntry.platform,
            ),
        };
    }

    /*
     * Checks for "watched" status
     */
    queryWatched(collectionEntry) {
        const ret = { hit: true };
        if (
            collectionEntry.watched === undefined ||
            collectionEntry.watched === true
        ) {
            ret.hit = false;
        }
        return ret;
    }

    /*
     * Queries for matches in "groups"
     */
    queryGroup(collectionEntry, group) {
        return {
            hit: this.inArray(collectionEntry.bSourceList, group),
        };
    }

    /*
     * Checks if value exists in the array arr
     */
    inArray(arr, value) {
        return arr.includes(value);
    }

    /*
     * Checks if any of the values in arr1 exists in arr2
     */
    inArrays(arr1, arr2) {
        return arr1.some((value) => arr2.includes(value));
    }

    /*
     * Checks if any of the elements in arr exists as a substring of str
     */
    arrayInString(arr, str) {
        for (const entryI in arr) {
            if (str.indexOf(arr[entryI]) !== -1) {
                return true;
            }
        }
        return false;
    }
}
